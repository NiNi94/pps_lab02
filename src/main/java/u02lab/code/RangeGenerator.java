package u02lab.code;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small u02lab.code.test, create code that pass u02lab.code.test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to u02lab.code.test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator extends AbstractSequenceGenerator {
    private int start;
    private int stop;

    public RangeGenerator(int start, int stop) {
        this.stop = stop;
        this.start = start;
        this.iterator = IntStream.range(start,stop).iterator();
    }

    @Override
    void generateIterator() {
        this.iterator = IntStream.range(this.start,this.stop).iterator();
    }
}
