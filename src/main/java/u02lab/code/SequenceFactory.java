package u02lab.code;

public class SequenceFactory {

    public static SequenceGenerator createRangeGenerator(int start, int stop) {
       return  new RangeGenerator(start, stop);
    }

    public static SequenceGenerator createRandomGenerator(int n) {
        return new RandomGenerator(n);
    }
}
