package u02lab.code;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Step 2
 *
 * Now consider a RandomGenerator. Using TDD approach (create small u02lab.code.test, create code that pass u02lab.code.test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to u02lab.code.test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (RandomGenerator vs RangeGenerator), using patterns as needed
 * - be sure tests still pass
 * - refactor the u02lab.code.test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator extends AbstractSequenceGenerator {
    private Random random;
    private List<Integer> randomSequence;

    public RandomGenerator(int n) {
        this.random = new Random();
        this.randomSequence = new ArrayList<>();
        this.generateRandomSequence(n);
        this.iterator = this.randomSequence.listIterator();
    }

    @Override
    void generateIterator() {
        this.randomSequence = new ArrayList<>();
        this.generateRandomSequence(randomSequence.size());
    }

    protected void generateRandomSequence(int length) {
        IntStream.range(0, length).forEach(f -> {
            if (random.nextDouble() < 0.5) {
                randomSequence.add(0);
            } else {
                randomSequence.add(1);
            }
        });
    }
}
