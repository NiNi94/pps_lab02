package u02lab.code;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractSequenceGenerator implements SequenceGenerator {
    protected  Iterator<Integer> iterator;

    @Override
    public Optional<Integer> next() {
        if (!isOver()) {
            return Optional.of(this.iterator.next());
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.generateIterator();
    }

    @Override
    public boolean isOver() {
        return !this.iterator.hasNext();
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> listOut = new ArrayList<>();
        this.iterator.forEachRemaining(t -> listOut.add(t));
        return listOut;
    }

    abstract void generateIterator();
}
