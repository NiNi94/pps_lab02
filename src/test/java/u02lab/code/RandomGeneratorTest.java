package u02lab.code;



import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class RandomGeneratorTest {
    private SequenceGenerator randomGenerator;


    @Test
    public void callNextOnemptySequenceReturnEmpty() {
        this.randomGenerator = SequenceFactory.createRandomGenerator(0);
        assertTrue(this.randomGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void callNextTwoTimesOnSequenceReturn0or1() {
        this.randomGenerator = SequenceFactory.createRandomGenerator(5);
        this.randomGenerator.next();
        this.randomGenerator.next();
        int elem = this.randomGenerator.next().get();
        assertTrue( elem == 0 || elem == 1 );
    }

    @Test
    public void produceListOfRemainElements() {
        this.randomGenerator = SequenceFactory.createRandomGenerator(5);
        this.randomGenerator.next();
        this.randomGenerator.next();

        List<Integer> remainList = this.randomGenerator.allRemaining();
        assertTrue(remainList.size() == 3);
    }
}
