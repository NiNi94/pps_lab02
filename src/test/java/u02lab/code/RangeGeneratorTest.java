package u02lab.code;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;


public class RangeGeneratorTest {
    private SequenceGenerator rangeGenerator;

    @org.junit.Before
    public void setUp() throws Exception {
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,0);
    }

    @Test
    public void emptyRangeGenerateEmptyOptional() {
        assertTrue(this.rangeGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void emptyRangeIsOver() {
        this.rangeGenerator.next();
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void generatorReturn2After2Iteration(){
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,4);
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        assertTrue(this.rangeGenerator.next().get() == 2);
    }


    @Test
    public void callingResetRestartGenerator() {
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,3);
        int first = this.rangeGenerator.next().get();
        this.rangeGenerator.next();
        this.rangeGenerator.reset();
        assertTrue(this.rangeGenerator.next().get() == first);
    }

    @Test
    public void isOverIsTrueAtTheEnd() {
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,3);
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void isOverIsFalseInTheMiddle() {
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,3);
        this.rangeGenerator.next();
        this.rangeGenerator.next();
        assertTrue(!this.rangeGenerator.isOver());
    }

    @Test
    public void produceListOfRemainElements() {
        this.rangeGenerator = SequenceFactory.createRangeGenerator(0,5);
        List<Integer> tempRemainList = new ArrayList<>((Arrays.asList(1,2,3,4,5)));
        this.rangeGenerator.next();

        List<Integer> remainList = this.rangeGenerator.allRemaining();
        assertTrue(listEquals(remainList, tempRemainList));

    }

    private boolean listEquals(List<Integer> A, List<Integer> B) {
        for (int i = 0; i < A.size(); i++) {
            if (!A.get(i).equals(B.get(i))) {
                return false;
            }
        }
        return true;
    }
}
